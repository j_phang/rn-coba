import React, { Component } from 'react'
import { Alert, ImageBackground, ToastAndroid } from 'react-native';
import {
    Container,
    Header,
    Title,
    Button,
    Left,
    Body,
    Icon,
    Text,
    Input,
    Item,
    Label,
    View,
    CheckBox
} from 'native-base'
import { withNavigation } from 'react-navigation';
import { ScrollView } from 'react-native-gesture-handler';
import styles from './style.js'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage';

class home extends Component {

    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            isChecked: false
        }
    }

    username = (event) => {
        this.setState({
            username: event.target.value
        })
    }

    password = (event) => {
        this.setState({
            password: event.target.value
        })
    }

    showAlert1 = (wow) => {
        Alert.alert(
            'Log In',
            `${wow}`,
            [
                { text: 'OK', onPress: () => console.log('OK Pressed') },
            ]
        );
    }

    async login() {
        // const { username, password } = this.state
        // try {
        //     const api = await Axios({
        //         method: 'post',
        //         headers: {
        //             'Content-Type': 'application/json'
        //         },
        //         url: 'http://ec2-3-0-98-199.ap-southeast-1.compute.amazonaws.com:3000/auth/',
        //         data: JSON.stringify({
        //             username,
        //             password
        //         })
        //     })
        //     if (api.data.token) {
        //         await AsyncStorage.setItem('@token', api.data.token)
        //         ToastAndroid.show('Bisa Login Harusnya', ToastAndroid.SHORT)
        //         console.log(`Bisa Log In Sayang :*`)
        //         console.log(api.data.token)
        //         console.log(this.state.username)
        //         console.log(this.state.password)
        //         console.log(this.state)
        //         this.props.navigation.navigate('Login')
        //     }
        // }
        // catch (e) {
        //     console.log(e.response.data)
        //     ToastAndroid.show(`wew`, ToastAndroid.SHORT)
        // }

        try {
            const postData = async (objParam, token) => await axios.post(
                `http://ec2-3-0-98-199.ap-southeast-1.compute.amazonaws.com:3000/auth/`, objParam, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            )

            postData({
                username: this.state.username,
                password: this.state.password
            })
                .then(response => {
                    AsyncStorage.setItem('@token', response.data.token)
                    ToastAndroid.show('Selamat Anda Dapat melakukan Login', ToastAndroid.SHORT)
                    console.log(`Bisa Log In Sayang :*`)
                    console.log(response.data.token)
                    console.log(this.state.username)
                    console.log(this.state.password)
                    this.setState({
                        username: '',
                        password: ''
                    })
                    this.props.navigation.navigate('Loggedin')
                })
                .catch(e => {
                    this.showAlert1('Password salah sayang :(')
                })
        }
        catch (e) {
            this.showAlert1(e)
        }
    }

    render() {
        return (
            <Container>
                <Header style={styles.header}>
                    <Left>
                        <Button transparent>
                            <Icon name='menu' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Login Page Sayang</Title>
                    </Body>
                </Header>

                <ImageBackground
                    source={{ uri: 'https://iphone-wallpaper.pics/wallpaper/i/p/iphone-wallpaper-space-011_3dc2eee8b71789b476b888c0bd38ba43_raw.jpg' }}
                    style={{ width: '100%', height: '100%' }}
                >
                    <ScrollView >
                        <View style={styles.Wrapper}>
                            <Item floatingLabel style={styles.input}>
                                <Label>
                                    <Text style={styles.text}>
                                        username Sayang
                                </Text>
                                </Label>
                                <Input keyboardType="default" value={this.state.username} onChangeText={(text) => this.setState({ username: text })} style={styles.inputField} />
                            </Item>

                            <Item floatingLabel style={styles.input}>
                                <Label>
                                    <Text style={styles.text}>
                                        Password Sayang
                                    </Text>
                                </Label>
                                <Input secureTextEntry={true} value={this.state.password} onChangeText={(text) => this.setState({ password: text })} style={styles.inputField} />
                            </Item>

                            <Item style={styles.button}>
                                <Button
                                    rounded
                                    onPress={
                                        () => this.login()
                                    }
                                    activeOpacity={.7}
                                >
                                    <Icon name='people' />
                                    <Text style={styles.text}>
                                        Login
                                    </Text>
                                </Button>

                                <CheckBox
                                    style={styles.check}
                                    checked={this.state.isChecked}
                                    onPress={() => {
                                        this.setState({
                                            isChecked: !this.state.isChecked
                                        })
                                    }}
                                />
                                <Text style={styles.texts}>
                                    Ingat saya
                                </Text>
                            </Item>

                            <Item style={styles.button}>
                                <Button rounded transparent
                                    onPress=
                                    {
                                        () => {
                                            this.props.navigation.navigate('Details', { username: this.state.username, password: this.state.password })
                                            this.setState({
                                                username: '',
                                                password: ''
                                            })
                                        }
                                    }
                                >
                                    <Text style={styles.text}>
                                        Belum daftar?
                                    </Text>
                                </Button>
                            </Item>
                        </View>
                    </ScrollView>
                </ImageBackground>
            </Container>
        )
    }
}
export default withNavigation(home)