import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
    header: {
      color: '#15183C'
    },
    inputField: {
      color: 'white',
      borderColor: 'white',
      marginTop: 5,
      width: '95%'
    },
    Wrapper: {
      display: 'flex',
      justifyContent: 'center',
      alignContent: 'center',
      width: '100%',
      height: '100%'
    },
    text: {
      color: 'white',
      fontSize: 20,
      display: "flex",
      justifyContent: "center",
      alignContent: "center"
    },
    texts: {
      color: 'white',
      fontSize: 10,
      display: "flex",
      justifyContent: "center",
      alignContent: "center"
    },
    input: {
      display: "flex",
      padding: 0,
      margin: 10,
      width: `100%`,
      justifyContent: "center"
    },
    label: {
      width: 80
    },
    button: {
      display: "flex",
      borderColor: 'transparent',
      margin: 10,
      justifyContent: "center"
    },
    check: {
      margin: 20
    },
    container: {
      display: 'flex',
      justifyContent: 'center',
      alignContent: 'center'
    }
  })

export default styles