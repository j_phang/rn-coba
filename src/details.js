import React, { Component } from 'react'
import { Alert, ImageBackground } from 'react-native';
import {
    Container,
    Header,
    Title,
    Button,
    Left,
    Body,
    Icon,
    Text,
    Input,
    Item,
    Label,
    Content,
    View
} from 'native-base'
import { withNavigation } from 'react-navigation';
import styles from './style.js'

class details extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            nama: '',
            phone: ''
        }
    }

    componentDidMount() {
        this.setState({
            email: this.props.navigation.getParam('username', 'noEmail'),
            password: this.props.navigation.getParam('password', 'NoPass')
        })
    }

    showAlert1 = () => {
        Alert.alert(
            'Log In',
            `Belum bisa signup sayang ${this.state.email}, ${this.state.password}, ${this.state.nama}, ${this.state.phone}`,
            [
                { text: 'OK', onPress: () => console.log('OK Pressed') },
                // { text: 'Cancel', onPress: () => console.log('Menolacc') }
            ]
        );
    }
    render() {
        return (
            <Container>
                <Header style={styles.header}>
                    <Left>
                        <Button transparent
                            onPress={
                                () => {
                                    this.props.navigation.pop()
                                    this.setState({
                                        email: '',
                                        password: ''
                                    })
                                }
                            }
                        >
                            <Icon name='arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Signup Page Sayang</Title>
                    </Body>
                </Header>

                <ImageBackground
                    source={{ uri: 'https://images.unsplash.com/photo-1500534314209-a25ddb2bd429?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80' }}
                    style={{ width: '100%', height: '100%' }}
                >
                    <Content >
                        <View style={styles.Wrapper}>
                            <Item floatingLabel style={styles.input}>
                                <Label>
                                    <Text style={styles.text}>
                                        Email Sayang
                                    </Text>
                                </Label>
                                <Input keyboardType="email-address" value={this.state.email} onChangeText={(text) => this.setState({ email: text })} style={styles.inputField} />
                            </Item>

                            <Item floatingLabel style={styles.input}>
                                <Label>
                                    <Text style={styles.text}>
                                        Password Sayang
                                    </Text>
                                </Label>
                                <Input secureTextEntry={true} value={this.state.password} onChangeText={(text) => this.setState({ password: text })} style={styles.inputField} />
                            </Item>

                            <Item floatingLabel style={styles.input}>
                                <Label>
                                    <Text style={styles.text}>
                                        Nama kamu siapa?
                                    </Text>
                                </Label>
                                <Input secureTextEntry={false} value={this.state.nama} onChangeText={(text) => this.setState({ nama: text })} style={styles.inputField} />
                            </Item>

                            <Item floatingLabel style={styles.input}>
                                <Label>
                                    <Text style={styles.text}>
                                        Nomormu dong ~
                                    </Text>
                                </Label>
                                <Input keyboardType="number-pad" value={this.state.phone} onChangeText={(text) => this.setState({ phone: text })} style={styles.inputField} />
                            </Item>

                            <Item style={styles.button}>
                                <Button rounded onPress={this.showAlert1} activeOpacity={.7}>
                                    {/* <Icon name='people' /> */}
                                    <Text style={styles.text}>
                                        Daftar!
                                    </Text>
                                </Button>
                            </Item>
                        </View>
                    </Content>
                </ImageBackground>
            </Container>
        )
    }
}

export default withNavigation(details)