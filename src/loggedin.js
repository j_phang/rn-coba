import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { withNavigation } from 'react-navigation';
import { Container, Header, Left, Button, Icon, Body, Title, } from 'native-base';
import styles from './style.js'
import AsyncStorage from '@react-native-community/async-storage';

class login extends Component {
    render() {
        const token = JSON.stringify(AsyncStorage.getItem('@token'))
        return (
            <Container>
                <Header style={styles.header}>
                    <Left>
                        <Button transparent
                            onPress={
                                () => {
                                    this.props.navigation.pop()
                                    this.setState({
                                        email: '',
                                        password: ''
                                    })
                                }
                            }
                        >
                            <Icon name='arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Home Page Sayang</Title>
                    </Body>
                </Header>
                <View>
                    <Text> Bisa Login sayang </Text>
                    <Text>{token}</Text>
                </View>
            </Container>
        )
    }
}
export default withNavigation(login)