import React, { Component } from 'react'
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Details from './src/details.js'
import Home from './src/home.js'
import Login from './src/loggedin.js';

class HomeScreen extends Component {
  render() {
    return <Home />
  }
}

class DetailsScreen extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return <Details />
  }
}

const StackNav = createStackNavigator({
    Home: {
      screen: HomeScreen
    },
    Details: {
      screen: DetailsScreen
    },
    Loggedin: {
      screen: Login
    }
  },
  {
    initialRouteName: "Home",
    headerMode: 'none'
  })

const AppContainer = createAppContainer(StackNav)

export default class App extends Component {
  render() {
    return <AppContainer />
  }
}